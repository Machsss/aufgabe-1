package com.company;

public class Car extends Main{

    String color;
    String serialNumber;
    double fuelCapacity;
    double fuel;
    double fuelConsumption;

    public Car(String color, String serialNumber, double fuelConsumption, double fuelCapacity) {
        this.color = color;
        this.serialNumber = serialNumber;
        this.fuelConsumption = fuelConsumption;
        this.fuelCapacity = fuelCapacity;
    }

    public void tankCar(double fuel) {
        this.fuel = fuel;
        System.out.println("Betankt mit " + fuel + " Liter");
    }

    public void breakCar(){
        System.out.println("Ich bremse");
    }

    public void superBoostMode(){
        if(fuel >= fuelCapacity * 0.1){
            System.out.println("Super Boost Mode");
        }
        else
        {
            System.out.println("Not enough Fuel");
        }
    }

    public void honk(int honks){
        while(honks > 0){
            System.out.println("TUUUUT");
            honks --;
        }
    }

    public void remaningRange(){
        double x = fuel / fuelConsumption;

        System.out.println("Ich kann noch " + x + " Kilometer fahren");
    }

}
